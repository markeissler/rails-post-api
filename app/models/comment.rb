class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true

  has_many :comments, as: :commentable, dependent: :destroy

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true
end
