class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy

  validates :name, presence: true, length: { maximum: 50 }
  validates :city, presence: true, length: { maximum: 50 }
end
