class Image < ActiveRecord::Base
  belongs_to :post

  validates :post_id, presence: true
  validates :url, presence: true, length: { maximum: 255 }
end
