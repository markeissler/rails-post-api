class PostsController < ApplicationController
  before_filter :find_post, only: [:show, :update, :destroy]
  respond_to :json

  include ApplicationHelper

  def index
    # need a pager in here!
    @posts = Post.last(30)
    @posts ||= []
    respond_with @posts
  end

  def show
    @post ||= {}
    if @post.blank?
      respond_with @post
    else
      respond_with @post.as_json.merge(:images => @post.images)
    end
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      respond_with @post
    else
      render_error("on create", @post.errors)
    end
  end

  def update
    if @post.update_attributes(post_params)
      respond_with @post
    else
      render_error("on update", @post.errors)
    end
  end

  def destroy
    if @post.nil?
      render_error("on destroy", "invalid post id")
      return
    end
    if @post.destroy
      respond_with @post
    else
      render_error("on destroy", @post.errors)
    end
  end

  private
    def find_post
      @post = Post.find_by(id: params[:id])
    end

    def post_params
      params.require(:post).permit(:user_id, :title, :content)
    end
end
