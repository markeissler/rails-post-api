class CommentsController < ApplicationController
  before_filter :find_comment, only: [:show, :destroy, :index_nested]
  before_filter :find_post, only: [:index, :create]
  respond_to :json

  include ApplicationHelper

  def index
    # need a pager in here!
    @comments = []
    unless @post.nil?
      @comments = @post.comments.last(30)
    end
    respond_with @comments
  end

  def show
    @comment ||= {}
    if @comment.blank?
      respond_with @comment
    else
      respond_with @comment.as_json.merge(:comments => nested_comments(@comment))
    end
  end

  def create
    if @post.nil?
      render_error("on create", "invalid post id")
      return
    end

    @comment = @post.comments.new(comment_params)
    if @comment.save
      respond_with @comment
    else
      render_error("on create", @comment.errors)
    end
  end

  def index_nested
    # need a pager in here!
    @comments = []
    unless @comment.nil?
      @comments = @comment.comments.last(30)
    end
    respond_with @comments
  end

  def create_nested
    @comment = Comment.new(comment_params)
    @comment.commentable_id = params[:id]
    @comment.commentable_type = "Comment"
    if @comment.save
      respond_with @comment
    else
      render_error("on create", @comment.errors)
    end
  end

  def destroy
    if @comment.nil?
      render_error("on destroy", "invalid comment id")
      return
    end
    if @comment.destroy
      respond_with @comment
    else
      render_error("on destroy", @comment.errors)
    end
  end

  private
    def find_comment
      @comment = Comment.find_by(id: params[:id])
    end

    def find_post
      @post = Post.find_by(id: params[:post_id])
    end

    def comment_params
      params.require(:comment).permit(:entity, :title, :content)
    end

    def nested_comments(comment)
      return [] if comment.blank?

      nest = comment.comments.map do |c|
        c.as_json.merge(:comments => nested_comments(c))
      end
    end
end
