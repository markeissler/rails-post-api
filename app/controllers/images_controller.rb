class ImagesController < ApplicationController
  before_filter :find_image, only: [:show, :destroy]
  before_filter :find_post, only: [:index, :create]
  respond_to :json

  include ApplicationHelper

  def index
    # need a pager in here!
    @images = []
    unless @post.nil?
      @images = @post.images.limit(30)
    end
    respond_with @images
  end

  def show
    @image ||= {}
    respond_with @image
  end

  def create
    if @post.nil?
      render_error("on create", "invalid post id")
      return
    end

    @image = @post.images.new(image_params)
    if @image.save
      respond_with @image
    else
      render_error("on create", @image.errors)
    end
  end

  def destroy
    if @image.nil?
      render_error("on destroy", "invalid image id")
      return
    end
    if @image.destroy
      respond_with @image
    else
      render_error("on destroy", @image.errors)
    end
  end

  private
    def find_image
      @image = Image.find_by(id: params[:id])
    end

    def find_post
      @post = Post.find_by(id: params[:post_id])
    end

    def image_params
      params.require(:image).permit(:post_id, :url)
    end
end
