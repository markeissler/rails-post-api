module ApplicationHelper
  def render_error(key, value)
    error = {
      "error_key" => key,
      "error_value" => value
      }
    render json: error.to_json, status: :unprocessable_entity
  end
end
