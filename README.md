# Post API Exercise

A simple blog post API app implemented in [Ruby on Rails 4.2](http://rubyonrails.org).

## Setup

```
>rake db:seed
```

## API Access
The API is exposed through the following URI pattern:

```
	http://WEBSITE.com/REST/PATH
```

For example, to retrieve all posts:

```
	http://WEBSITE.com/posts.json
```

To retrieve all comments for post 12:

```
	http://WEBSITE.com/posts/12/comments.json
```

## JSON requests
Access to the API is by json requests only: that means endpoints must be accessed with a ".json" format specified:

	http://WEBSITE.com/posts.json
	
If the endpoint is accessed with any other filename extension, a server error is thrown (obviously, this could be improved, perhaps to return a **422 "Unprocessable entity"** error instead).

## REST paths
The following paths have been implemented:

```
          Prefix Verb   URI Pattern                            Controller#Action
   post_comments GET    /posts/:post_id/comments(.:format)     comments#index
                 POST   /posts/:post_id/comments(.:format)     comments#create
    post_comment GET    /posts/:post_id/comments/:id(.:format) comments#show
                 DELETE /posts/:post_id/comments/:id(.:format) comments#destroy
     post_images POST   /posts/:post_id/images(.:format)       images#create
      post_image GET    /posts/:post_id/images/:id(.:format)   images#show
                 DELETE /posts/:post_id/images/:id(.:format)   images#destroy
           posts GET    /posts(.:format)                       posts#index
                 POST   /posts(.:format)                       posts#create
            post GET    /posts/:id(.:format)                   posts#show
                 PATCH  /posts/:id(.:format)                   posts#update
                 PUT    /posts/:id(.:format)                   posts#update
                 DELETE /posts/:id(.:format)                   posts#destroy
         comment GET    /comments/:id(.:format)                comments#show
           image GET    /images/:id(.:format)                  images#show
comment_comments GET    /comments/:id/comments(.:format)       comments#index_nested
                 POST   /comments/:id/comments(.:format)       comments#create_nested
```

## Database and Seed Data
This app was built using sqlite3 as the database engine. It could just as easily be deployed with an alternative SQL backend (e.g. Postgres) by changing the Gemfile and updating the configuration at `config/database.yml`.

The seed file will generate sample data via the [Faker](https://github.com/stympy/faker) ruby gem. The following commands will prepare the database and import the data:

	>rake db:migrate
	>rake db:seed
	
You can clear out and reload all data as well if the seed file is changed:

	>rake db:drop
	>rake db:migrate
	>rake db:seed

## Data Destroy (Dependencies)
Through the _has_many_ and _belongs_to_ associations, a particular Post record's Comment and Image records are destroyed when the corresponding Post record is destroyed. In real life, you might actually want to disallow deletes in this way and instead mark a Post/Comment as deleted, otherwise threaded comments will lose context.

## Differences from Spec
The Spec was interpreted to such that Post, Comment, and Image resources should be exposed through the API but the User resource should not. It would be more complete to expose the User interface as well since the ownership hierarchy is as follows:

```
	User<-Post<-Comment<-Comment
	          <-Image
```

As per the Spec, then, both Images and Comments are handled in the context of the owning Post:

```
	POST/PATCH http://WEBSITE.com/posts/12/comments.json
	POST/PATCH http://WEBSITE.com/posts/12/images.json
```

And:

```
	DELETE http://WEBSITE.com/posts/12/comments/43.json
	DELETE http://WEBSITE.com/posts/12/images/3.json
```

The obvious advantage when accessing the API this way is that we leverage the natural belongs_to and has_many relationships between objects.

When creating a Post, the owning user_id must be specified in the json parameters that are POSTd.

While the API allows for Comment and Image objects to be retrieved directly:

```
	GET http://WEBSITE.com/images/12.json
	GET http://WEBSITE.com/comments/43.json
```

As mentioned above, other paths have note been exposed because it would require the caller to provide a post_id in the json parameters that are POSTd. Certainly, both direct access paths along with relationship paths could be offered for a greater level of convenience/flexibility.

## Fixes (v1.1.0)
This version fixes omissions noted in the first release:

* Returned Comment objects now include threaded Comments. This might not be a good idea in real world use because the threaded comments aren't paged. It would probably be better to stick to the previous method of specifically requesting a list of Comment.comments.

This version also adds this missing feature (from the spec) in the first release:

* Returned Post objects now include a list of associated Images.

## Todo list
There is so much to do to make this a proper API:

- **versioning**: APIs should be versioned (e.g. http://myapi.com/api/v1/endpoint/).
- **authorization**: Access is wide open, it should be secured with tokens or passwords.
- **error handling**: Much better error handling is needed to return appropriate responses always.
- **expose User resource**: Not really sure why this isn't exposed in the spec.
- **unit tests**: I had planned to add them from the beginning but then changed my mind since their was a time limit and I didn't want to get bogged down.

## Requirements
The app was developed on OSX, with Ruby on Rails v4.2 and ruby v2.3.3.
