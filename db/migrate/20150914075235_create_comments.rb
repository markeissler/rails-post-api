class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :title
      t.text :content
      t.belongs_to :commentable, index: true, polymorphic: true

      t.timestamps null: false
    end
  end
end
