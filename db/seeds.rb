# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
I18n.enforce_available_locales = true

printf "\nSeed initiated! \n\n"

printf "\nDropping all users\n"

User.unscoped.delete_all

printf "\nCreating new users: \n"
99.times do |n|
  name = Faker::Name.name
  city = Faker::Address.city
  User.create!(
    name: name,
    city: city
  )
end

printf "\Dropping all posts, comments, images\n"

Post.unscoped.delete_all
Comment.unscoped.delete_all
Image.unscoped.delete_all

printf "\nCreating new posts, comments, images\n"

users = User.limit(10)
50.times do
  users.each do |user|
    title = Faker::Lorem.sentence(2)
    content = Faker::Lorem.sentence(5)
    post = user.posts.create(title: title, content: content)

    # add an image url to the post too!
    post.images.create(url: Faker::Avatar.image)

    # add a comment to the post too!
    commentTitle = Faker::Lorem.sentence(2)
    commentContent = Faker::Lorem.sentence(5)
    comment1 = post.comments.create(title: commentTitle, content: commentContent)

    # add a comment to the comment!
    commentTitle = Faker::Lorem.sentence(2)
    commentContent = Faker::Lorem.sentence(5)
    comment2 = comment1.comments.create(title: commentTitle, content: commentContent)
  end
end

printf "\nSeed completed! \n\n"
